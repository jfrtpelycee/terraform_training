# Create the SSH key
resource "tls_private_key" "example_ssh" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

# Create the SSH key
resource "tls_private_key" "example_ssh_1" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

# Create the SSH key
resource "tls_private_key" "example_ssh_2" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

# Create the SSH key
resource "tls_private_key" "example_ssh_3" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "local_file" "ssh_key" {
  filename = "Private_SSH_Key.pem"
  content  = tls_private_key.example_ssh.private_key_pem
}

resource "local_file" "ssh_key_1" {
    filename = "Private_SSH_Key_1.pem"
    content  = tls_private_key.example_ssh_1.private_key_pem
}

resource "local_file" "ssh_key_2" {
    filename = "Private_SSH_Key_2.pem"
    content  = tls_private_key.example_ssh_2.private_key_pem
}

resource "local_file" "ssh_key_3" {
    filename = "Private_SSH_Key_3.pem"
    content  = tls_private_key.example_ssh_3.private_key_pem
}